/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.red61.graphql.test;

import com.red61.via.restful.model.NextPerformanceDetails;
import com.red61.via.restful.model.NextPerformanceResult;
import com.red61.via.restful.model.SearchResultDetails;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.eclipse.microprofile.graphql.Description;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Query;

/**
 *
 * @author will
 */
@GraphQLApi
public class DataResource {

	@Inject
	DataService dataService;
	
	@Query("nextPerformances")
	@Description("Get next performance list")
	public List<NextPerformanceDetails> getNextPerformanceDetails() {
		return dataService.getNextPerformanceDetails();
	}
	
	@Query("PerformanceSearch")
	public List<PerformanceSearchResults> getPerformanceSearchResults(String eventTitle) {
		return dataService.getPerformanceSearchResults(eventTitle);
	}
}
