/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.red61.graphql.test;

import com.red61.via.restful.client.AccessControlApi;
import com.red61.via.restful.client.CatApi;
import com.red61.via.restful.model.GetPerformanceSearchResultsRequest;
//import com.red61.via.restful.model.GetNextPerformancesRequest;
import com.red61.via.restful.model.LoginCredentials;
import com.red61.via.restful.model.NextPerformanceDetails;
import com.red61.via.restful.model.SearchResultDetails;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.RestClientBuilder;

/**
 *
 * @author will
 */
@RequestScoped // probably more app scoped
public class DataService {

//	@Inject
	private AccessControlApi accessControlApi;
	private AuthApi authApi;
	private CatApi catApi;
	
	// Dummy data
	private int mainRoleId = 5;
	private int roleId = 1;
	private int locationId = 1;
	private int roleServerId = 1030;

	@PostConstruct
	public void init() {
//		String baseUrl = "http://172.19.0.6:8080/peter2/api/restful";
		String baseUrl = "http://localhost:8080/peter2/api/restful";
		try {
			authApi = RestClientBuilder.newBuilder().baseUri(new URI(baseUrl)).build(AuthApi.class);
			LoginCredentials loginCredentials = new LoginCredentials();
			loginCredentials.setUsername("mcn100");
			loginCredentials.setPassword("red61rocks");
			loginCredentials.setLocationId(1);
			Response login = authApi.login(loginCredentials);
			LoginResponse loginResponse = login.readEntity(LoginResponse.class);
			// .getHeaderString(HttpHeaders.AUTHORIZATION);

			RequestFilterJWT filterJWT = new RequestFilterJWT("Bearer " + loginResponse.getJwt());

			accessControlApi = RestClientBuilder.newBuilder().register(filterJWT).baseUri(new URI(baseUrl)).build(AccessControlApi.class);
			catApi = RestClientBuilder.newBuilder().register(filterJWT).baseUri(new URI(baseUrl)).build(CatApi.class);
		}
		catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	List<NextPerformanceDetails> getNextPerformanceDetails() {
		return accessControlApi.getNextPerformanceList(9999);
	}

	public List<PerformanceSearchResults> getPerformanceSearchResults(String eventTitle) {
		List<PerformanceSearchResults> results = new ArrayList<>();
		GetPerformanceSearchResultsRequest request = new GetPerformanceSearchResultsRequest(); // Why no constructor with required params?!
		request.setRoleServerId(roleServerId);
		request.setMainRoleId(mainRoleId);
		request.setRoleId(roleId);
		request.setListPastPerformances(Boolean.TRUE);
		request.setListPastPerformancesAllowance(0);
		request.setServerFlag(0);
		
		request.setEventString(eventTitle);
		request.setVenueString("");
		request.setSubvenueString("");
		request.setCompanyString("");
		request.setPromoterString("");
		request.setEventTagName("");
		request.setEventTypeName("");
		
		List<SearchResultDetails> nextPerformanceResult = catApi.getPerformanceSearchResults(request);
		for(SearchResultDetails searchResultDetails : nextPerformanceResult) {
			results.add(new PerformanceSearchResults(searchResultDetails));
		}
		return results;
	}
	
//	public enum SortCriteria {
//		PERFORMANCE_DATE_ASC,
//		PERFORMANCE_DATE_DESC,
//		EVENT_TITLE_ASC,
//		EVENT_TITLE_DESC,
//		VENUE_TITLE_ASC,
//		VENUE_TITLE_DESC,
//		AVAILABILITY_ASC,
//		AVAILABILITY_DESC;
//	}
//	
//	public enum Status {
//
//		/** Added a performance to the gui list ready for copying */
//		ADDED("Added"),
//		NEW("New"),
//		/** Structure selected */
//		PRICED("Priced"),
//		INVENTORY_CREATED("Inventory Created"),
//		OFF_SALE("Off Sale"),
//		ON_SALE("On Sale"),
//		CANCELLED("Cancelled"),
//		STOPPED("Stopped"),
//		ARCHIVED("Archived");
//		private final String displayName;
//
//		private Status(String displayName) {
//			this.displayName = displayName;
//		}
//
//		@Override
//		public String toString() {
//			return displayName;
//		}
//
//		public static Status fromDisplayName(String displayName) {
//			for (Status s : values()) {
//				if (s.displayName.equals(displayName)) {
//					return s;
//				}
//			}
//			throw new IllegalArgumentException("Couldn't find status for: " + displayName);
//		}
//		/**
//		 * The set of performance statuses that mean that a performance has
//		 * had inventory generated.
//		 */
//		public static final Set<Status> INVENTORY_GENERATED = unmodifiableSet(
//				EnumSet.of(INVENTORY_CREATED, OFF_SALE, ON_SALE, CANCELLED, STOPPED, ARCHIVED));
//	}
}
