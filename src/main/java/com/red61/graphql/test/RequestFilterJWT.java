/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.red61.graphql.test;

import java.io.IOException;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.HttpHeaders;

/**
 *
 * @author will
 */
class RequestFilterJWT implements ClientRequestFilter {

	private final String header; 
	
	RequestFilterJWT(String header) {
		this.header = header;
	}

	@Override
	public void filter(ClientRequestContext crc) throws IOException {
		crc.getHeaders().add(HttpHeaders.AUTHORIZATION, header);
	}
	
}
